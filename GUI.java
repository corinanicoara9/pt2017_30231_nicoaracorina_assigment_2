package poli2;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import java.lang.reflect.Method;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JMenu;

import javax.swing.JMenuItem;
import javax.swing.JPanel;

import javax.swing.JMenuBar;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
public class GUI {

    class ServicesCarte
    {
        public static String cale = "D:\\bookStore\\document1.xml";


        public static List<Carte> DeserializeFromXML()
        {
            XmlSerializer deserializer = new XmlSerializer(typeof(List<Carte>));
            TextReader textReader = new StreamReader(@cale);
            List<Carte> carti; 
            carti = (List<Carte>)deserializer.Deserialize(textReader);
            textReader.Close();

            return carti;
        }

         public void adaugaCarteToXml(Carte c) 
        {
            List<Carte> listaCarti = DeserializeFromXML();
             Boolean este=false;
             Boolean gresitcota = false;
            foreach (Carte carte in listaCarti)
            {
                if (c.Cota == carte.Cota && c.Titlu==carte.Titlu && c.Autor==carte.Autor)
                {
                    este = true;
                    carte.NrCarti += c.NrCarti;
                }
                if (c.Cota == carte.Cota && c.Titlu != carte.Titlu && c.Autor != carte.Autor)
                    gresitcota = true;
                

            }
            if (este == false && gresitcota == false)
                listaCarti.Add(c);
            else if (gresitcota == true) MessageBox.Show("Mai exista o carte cu aceasta cota!");
            XmlSerializer serializer = new XmlSerializer(typeof(List<Carte>));
            TextWriter textWriter = new StreamWriter(@cale);
            serializer.Serialize(textWriter, listaCarti);
            textWriter.Close();
        }


        public void updateCarte(Carte carte)
        {

            List<Carte> listaCarti = DeserializeFromXML();
            //List<Carte> listaCarti = new List<Carte>();
            int cota = carte.Cota;
            foreach (Carte c in listaCarti)
            {
                if (c.Cota == cota)
                {
                    c.Titlu = carte.Titlu;
                    c.Autor = carte.Autor;
                    c.Gen = carte.Gen;
                    c.NrCarti = carte.NrCarti;
                    c.Pret = carte.Pret;
                }
            }
           
            XmlSerializer serializer = new XmlSerializer(typeof(List<Carte>));
            TextWriter textWriter = new StreamWriter(@cale);
            serializer.Serialize(textWriter, listaCarti);
            textWriter.Close();
        }

        public void stergeCarte(Carte carte)
        {

            List<Carte> listaCarti = DeserializeFromXML();
            int cota = carte.Cota;
            foreach (Carte c in listaCarti)
            {
                if (c.Cota == cota)
                {
                    c.NrCarti = 0;
                }
            }
           
            XmlSerializer serializer = new XmlSerializer(typeof(List<Carte>));
            TextWriter textWriter = new StreamWriter(@cale);
            serializer.Serialize(textWriter, listaCarti);
            textWriter.Close();
        }

        

       


        public DataTable getCarte(Carte data)
        {
            DataTable dataTable = new DataTable(typeof(Carte).Name);
            PropertyInfo[] props = typeof(Carte).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in props)
            {
                dataTable.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ??
                    prop.PropertyType);
            }

          //  foreach (Carte item in data)
            //{
                var values = new object[props.Length];
                for (int i = 0; i < props.Length; i++)
                {
                    values[i] = props[i].GetValue(data, null);
                }
                dataTable.Rows.Add(values);
            //}
            return dataTable;
        }  


    }
}


}
