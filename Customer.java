package poli2;

public class Customer {


    class AngajatController
    {
        Admin ad;
        AngajatServices ma;
        FindAngajat fa;
        UserServices us;

        public AngajatController(Admin _ad)
        {
            this.ad = _ad;
            ma = new AngajatServices();
            fa = new FindAngajat();
            us = new UserServices();
        }



        public void updateAngajat(object sender, EventArgs e)
        {

            String[] str = new String[6];
            DataGridView d = ad.getTabelAngajati();
            int row = d.CurrentRow.Index;
            for (int i = 0; i < 6; i++)
            {
                str[i] = d.Rows[row].Cells[i].Value.ToString();
            }
            string pass = getMd5Hash(str[5]);
            Angajat a = new Angajat(str[0], str[1], str[2], Convert.ToInt32(str[3]), str[4], pass);
            ma.updateAngajatXml(a);

        }

        public void stergeAngajat(object sender, EventArgs e)
        {
            String[] str = new String[6];
            DataGridView d = ad.getTabelAngajati();
            int row = d.CurrentRow.Index;
            for (int i = 0; i < 6; i++)
            {
                str[i] = d.Rows[row].Cells[i].Value.ToString();
            }
            string pass = getMd5Hash(str[5]);
            Angajat a = new Angajat(str[0], str[1], str[2], Convert.ToInt32(str[3]), str[4], pass);
            ma.deleteAngajatFromXml(a);
            us.deleteUser(str[4]);
            //DataTable dt2 = fa.getAngajatTabel(a.Cnp);
            //ad.getTabelAngajati().DataSource = dt2;

        }

        public void cautaAngajatCnp(object sender, EventArgs e)
        {
            String cnp = ad.getCnpAngajat();
            if (cnp != "")
            {
                if (cnp.Length == 13)
                {

                    Angajat a = fa.getAngajatCnp(cnp);
                    DataTable dt2 = fa.getAngajatTabel(a);
                    ad.getTabelAngajati().DataSource = dt2;

                   
                }
                else MessageBox.Show("Cnp-ul trebuie sa fie de 13 cifre!");

            }
            else MessageBox.Show("Introduce-ti cnp-ul!");

        }

        public void insertAngajat(object sender, EventArgs e)
        {
            String[] str = new String[6];
            DataGridView d = ad.getTabelAngajati();
            int row = d.CurrentRow.Index;
            for (int i = 0; i < 6; i++)
            {
                str[i] = d.Rows[row].Cells[i].Value.ToString();
            }
            if (fa.findAngajatCnp(str[2]) == false)
            {

                string pass = getMd5Hash(str[5]);
                Angajat a = new Angajat(str[0], str[1], str[2], Convert.ToInt32(str[3]), str[4], pass);
                User u = new User(a.Username, a.Password, "Angajat");
                if (us.findUsername(u) == false)
                {
                    us.adaugaUserToXml(u);
                    ma.adaugaAngajatToXml(a);
                }
                else MessageBox.Show("Username-ul exista deja!");

            }
            else MessageBox.Show("Clientul exista deja!");
        }

        


        public String getMd5Hash(String input)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5 md5Hasher = MD5.Create();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

    }
}


}
