package poli2;

import java.awt.Color;
import java.awt.Panel;
import java.util.ArrayList;
import java.util.Comparator;
public class CashierQueue {


    class CarteController 
    {
        Admin ad;
        FindCarte fc;
        ServicesCarte sc;

        public CarteController(Admin _ad)
        {
            this.ad = _ad;
            fc = new FindCarte();
            sc = new ServicesCarte();
        }

       

        public void updateCarte(object sender, EventArgs e)
        {

            String[] str = new String[6];
            DataGridView d = ad.getTabelCarti();
            int row = d.CurrentRow.Index;
            for (int i = 0; i < 6; i++)
            {
                str[i] = d.Rows[row].Cells[i].Value.ToString();
            }
            Carte a = new Carte(str[0], str[1], str[2], Convert.ToInt32(str[3]), Convert.ToDouble(str[4]), Convert.ToInt32(str[5]));
           
           sc.updateCarte(a);
           

        }

        public void stergeCarte(object sender, EventArgs e)
        {
            String[] str = new String[6];
            DataGridView d = ad.getTabelCarti();
            int row = d.CurrentRow.Index;
            for (int i = 0; i < 6; i++)
            {
                str[i] = d.Rows[row].Cells[i].Value.ToString();
            }
            Carte a = new Carte(str[0], str[1], str[2], Convert.ToInt32(str[3]), Convert.ToDouble(str[4]), Convert.ToInt32(str[5]));
            sc.stergeCarte(a);

        }

        public void cautaCarte(object sender, EventArgs e)
        {
            String titlu = ad.getTitlu();
            String autor = ad.getAutor();
            if (autor != ""  && titlu!="")
            {
                Carte c = fc.getCarteTabel(titlu, autor);
                DataTable dt2 = sc.getCarte(c);
                ad.getTabelCarti().DataSource = dt2;
                
            }
            else MessageBox.Show("Introduce-ti titlu si autorul cartii!");

        }

        public void getRaport(object sender, EventArgs e)
        {
            //ReportFactory factory = new XmlReportFactory();
            //IReport button = factory.createReport();
            //button.generateReport();

            ReportFactory.GetInstance(ReportType.Xml).createReport().generateReport();

            DataTable dt2 = fc.listCarteToTable(fc.getoutOfStock());
            ad.getTabelRaport().DataSource = dt2;


        }

        public void insertCarte(object sender, EventArgs e)
        {
            String[] str = new String[6];
            DataGridView d = ad.getTabelCarti();
            int row = d.CurrentRow.Index;
            for (int i = 0; i < 6; i++)
            {
                str[i] = d.Rows[row].Cells[i].Value.ToString();
            }
           
                Carte a = new Carte(str[0], str[1], str[2], Convert.ToInt32(str[3]), Convert.ToDouble(str[4]), Convert.ToInt32(str[5]));
                sc.adaugaCarteToXml(a);

           
        }
        

    }
}


}
