package poli2;

import java.util.Collections;
import java.util.Random;
import java.util.ArrayList;



public class Simulation {


    class AdminOp
    {
        Admin administrator;
        AngajatController ang;
        CarteController carte;


        public AdminOp(Admin _administrator)
        {
            this.administrator = _administrator;
            ang = new AngajatController(administrator);
            carte = new CarteController(administrator);

        }

        public void butoane()
        {
            Button adAngajat = administrator.getInsertAngajatB();
            adAngajat.Click += new EventHandler(ang.insertAngajat);
            Button cautaAngajat = administrator.getButonCautaAngajat();
            cautaAngajat.Click += new EventHandler(ang.cautaAngajatCnp);
            Button stergeAng = administrator.getDeleteAngajatB();
            stergeAng.Click += new EventHandler(ang.stergeAngajat);
            Button updateAng = administrator.getUpdateAngajatB();
            updateAng.Click += new EventHandler(ang.updateAngajat);


            Button adCarte = administrator.getButonInsertCarte();
            adCarte.Click += new EventHandler(carte.insertCarte);
            Button cautaCarte = administrator.getButonCautaCarte();
            cautaCarte.Click += new EventHandler(carte.cautaCarte);
            Button stergeCarte = administrator.getButonDeleteCarte();
            stergeCarte.Click += new EventHandler(carte.stergeCarte);
            Button updateCarte = administrator.getButonUpdateCarte();
            updateCarte.Click += new EventHandler(carte.updateCarte);

            Button raport = administrator.getButonRaport();
            raport.Click += new EventHandler(carte.getRaport);

           



        }



    }
}



	

}
